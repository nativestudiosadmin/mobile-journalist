import React from 'react'
import moment from 'moment'
import { Alert, ActivityIndicator } from 'react-native'
import PushNotification from 'react-native-push-notification'
import { connect } from 'react-redux'
import { registerDevice } from '../actions/device.actions'
import { createNexxtvVideo } from '../api/apiClient'
import { updateVideoRecord } from '../database/dbClient'
import FullScreenContainer from '../components/Containers/FullScreenContainer'
import PushText from '../components/PushNotifications/PushText'
import PushButton from '../components/PushNotifications/PushButton'
import { getVideo } from '../api/apiClient'

class ProcessComponent extends React.Component {
  state = {
    lastCheck: moment().format('hh:mm:ss'),
    lastResponse: null,
    pushNotificationEnabled: false,
  }

  componentDidMount() {
    this.check()
  }

  check = async () => {
    if (this.cancelled) { return }

    this.setState({ lastCheck: moment().format('hh:mm:ss') })

    try {
      const { video, token } = this.props
      // const response = await video.getStatus()

      const response = await getVideo(video.remoteId, token)

      if (response && response.status === 'ready') {
        return this.next()
      }

    } catch (e) {
      console && console.log && console.log(e)
    }

    window.setTimeout(this.check, 5000)
  }

  // To be integrated with new api
  registerForPushNotification = () => {
    return new Promise((resolve, reject) => {
      PushNotification.configure({
        onRegister: (token) => {
          const { dispatch } = this.props

          dispatch(registerDevice(token.token))
            .then(({ device }) => {
              resolve(device)
            })
        },
        onError(error) {
          reject(error)
        },
        senderID: '867659078473',
        permissions: {
          alert: true,
          badge: true,
          sound: true,
        },
        popInitialNotification: true,
        requestPermissions: true,
      })
    })
  }

  enablePushNotification = async () => {
    try {
      const device = await this.registerForPushNotification()

      await createNexxtvVideo(device)

      this.setState({
        pushNotificationEnabled: true,
      })
    } catch (error) {
      Alert.alert(error.message)
    }
  }

  next = async () => {
    const { navigation, video, token } = this.props
    const { navigate } = navigation

    await updateVideoRecord(video, {
      state: 'ready',
      code: token
    })

    console.log("PROCESSED VIDEO")
    console.log(video)

    navigate('editor')
  }

  render() {
    const enablePushNotificationButton = this.state.pushNotificationEnabled ?
      null : <PushButton onEnableNotifications={() => {}}/> // to be updated with enablePushNotification when api is available

    // TODO: Fix size:80 for Android
    return (
      <FullScreenContainer>

        <ActivityIndicator size={1} color="#000" style={{position: 'absolute', top: 60, right: 60}}/>

        <PushText/>

        {enablePushNotificationButton}

      </FullScreenContainer>
    )
  }
}

const mapStateToProps = ({ video, token }) => {
  return { video, token }
}

export const Process = connect(mapStateToProps)(ProcessComponent)
