import React from 'react'
import RegularContainer from '../components/Containers/RegularContainer'
import EndText from '../components/End/EndText'
import EndFooter from '../components/End/EndFooter'


export class End extends React.Component {
  next = () => {
    const { navigate } = this.props.navigation
    navigate('start')
  }

  render() {
    return (
      <RegularContainer>

        <EndText/>

        <EndFooter goToStart={this.next}/>

      </RegularContainer>
    )
  }
}