import React from 'react'
import Video from 'react-native-video'
import { connect } from 'react-redux'
import RegularContainer from '../components/Containers/RegularContainer'
import SubtitlesContent from '../components/SubtitlesPlayer/SubtitlesContent'
import SubtitlesFooter from '../components/SubtitlesPlayer/SubtitlesFooter'


class SubtitlesPlayerComponent extends React.Component {
  state = {
    paused: false,
  }

  back = () => {
    this.setState({ paused: true })
    this.props.navigation.goBack()
  }

  next = () => {
    this.setState({ paused: true })
    this.props.navigation.navigate('channels')
  }

  render() {
    return (
      <RegularContainer>

        <SubtitlesContent videoUri={this.props.video.uri}/>

        <SubtitlesFooter
          onBackClick={this.back}
          onNextClick={this.next}
        />

      </RegularContainer>
    )
  }
}

const mapStateToProps = ({ video }) => {
  return { video }
}

export const SubtitlesPlayer = connect(mapStateToProps)(SubtitlesPlayerComponent)
