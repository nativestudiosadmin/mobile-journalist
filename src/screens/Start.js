import React from 'react'
import { connect } from 'react-redux'
import { authRequest } from '../api/apiClient'
import { updateToken } from '../actions/token.actions'
import Keyboard from '../components/Keyboard'
import StartContainer from '../components/Containers/StartContainer'
import CodeFooter from '../components/Code/CodeFooter'
import CodeText from '../components/Code/CodeText'
import CodeInputModal from '../components/Code/CodeInputModal'


class StartComponent extends React.Component {

  onCodeSubmit = response => {
    this.props.dispatch(updateToken(response['token']))
    this.next()
  }

  onCodeButtonPress = () => {
    this.refs.codeInputModal.show()
  }

  checkCode = async code => {
    const response = await authRequest(code)
    return response
  }

  next = () => {
    const { navigate } = this.props.navigation
    this.refs.codeInputModal && this.refs.codeInputModal.hide()
    navigate('videoSelector')
  }

  render() {
    return (
      <Keyboard>
        <StartContainer>

          <CodeText />

          <CodeFooter onCodeButtonPress={this.onCodeButtonPress} />

          <CodeInputModal
            ref="codeInputModal"
            onCodeSubmit={this.onCodeSubmit}
            onCheckCode={this.checkCode} />

        </StartContainer>
      </Keyboard>
    )
  }

}

const mapStateToProps = ({ device, token }) => {
  return { device, token }
}

export const Start = connect(mapStateToProps)(StartComponent)


