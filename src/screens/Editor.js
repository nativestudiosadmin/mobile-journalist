import React from 'react'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { NEXXTV_API } from '../config/api'
import Keyboard from '../components/Keyboard'
import RegularContainer from '../components/Containers/RegularContainer'
import EditorFooter from '../components/Editor/EditorFooter'
import EditorModal from '../components/Editor/EditorModal'
import { updateVideo } from '../api/apiClient'


class EditorComponent extends React.Component {
  state = {
    videoName: '',
    videoDescription: '',
  }

  componentDidMount() {
    const { video } = this.props

    this.setState({
      videoName: video.title,
      videoDescription: video.description,
    })
  }

  next = () => {
    const { navigate } = this.props.navigation
    navigate('subtitlesPlayer')
  }

  openNameEditor = () => {
    this.props.navigation.navigate('nameEditor')
  }

  openSubtitlesEditor = () => {
    const { navigate } = this.props.navigation
    navigate('subtitlesEditor')
  }

  onModalButtonPress = (modal) => {
    this.refs[modal].show()
  }


  saveMetaData = async () => {
    // const { video } = this.props
    // const { videoName, videoDescription } = this.state
    // const body = new FormData()

    // body.append('cmd', 'setmetadata')
    // body.append('authkey', video.code)
    // body.append('item', video.remoteId)
    // body.append('title', videoName)
    // body.append('description', videoDescription)

    // return fetch(NEXXTV_API, {
    //   method: 'post',
    //   body
    // }).then(response => response.json())
  }

  updateVideoRequest = async () => {
    const { video, token } = this.props
    const { videoName, videoDescription } = this.state

    try {
      //let response = await this.saveMetaData()
      //
      let response = await updateVideo(video.remoteId, token, { title: videoName, subtitle: videoDescription })

      console.log("VIDEO UPDATE")
      console.log(response)

      if (!response || response.result !== 'ok') {
        Alert.alert(response.reason)
      }
    } catch (e) {
      console && console.log && console.log(e)
    }
  }

  onValueChange = (field, value, finished) => {
    let state = this.state

    state[field] = value
    this.setState(state)

    if (finished) {
      this.refs[`${field}Modal`].hide()

      this.updateVideoRequest()
    }
  }

  render() {
    return (
      <Keyboard>
        <RegularContainer>

          <EditorFooter
            openSubtitlesEditor={this.openSubtitlesEditor}
            openDescriptionModal={() => this.onModalButtonPress('descriptionModal')}
            openNameModal={this.openNameEditor }
            goToChannels={this.next}/>

          <EditorModal ref="descriptionModal"/>

        </RegularContainer>
      </Keyboard>
    )
  }
}

const mapStateToProps = ({ video, token }) => {
  return { video, token }
}

export const Editor = connect(mapStateToProps)(EditorComponent)
