import React from 'react'
import Video from 'react-native-video'
import { connect } from 'react-redux'
import FullScreenContainer from '../components/Containers/FullScreenContainer'
import BackButton from '../components/VideoPlayer/BackButton'


class VideoPlayerComponent extends React.Component {
  state = {
    paused: false,
  }

  onBackClick = () => {
    this.setState({ paused: true })
    const { navigate } = this.props.navigation.goBack()
  }

  render() {
    return (
      <FullScreenContainer>

        <Video
          style={{height: '80%', width: "100%"}}
          resizeMode="contain"
          paused={this.state.paused}
          source={{uri: this.props.video.uri}}/>

        <BackButton onBackClick={this.onBackClick}/>

      </FullScreenContainer>
    )
  }
}

const mapStateToProps = ({ video }) => {
  return { video }
}

export const VideoPlayer = connect(mapStateToProps)(VideoPlayerComponent)
