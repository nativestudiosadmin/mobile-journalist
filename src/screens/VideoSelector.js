import React from 'react'
import { connect } from 'react-redux'
import { Platform, ToastAndroid } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { store } from '../config/store'
import { addVideo } from '../actions/video.actions'
import StartContainer from '../components/Containers/StartContainer'
import VideoText from '../components/VideoSelection/VideoText'
import VideoFooter from '../components/VideoSelection/VideoFooter'


export class VideoSelectorComponent extends React.Component {

  onVideoButtonPress = () => {
    const options = {
      title: 'Wähle ein Video',
      mediaType: 'video',
      videoQuality: 'high',
      allowsEditing: false,
    }

    ImagePicker.launchImageLibrary(options, (video) => {
      if (video.didCancel === true) {
        return this.cancelled()
      }

      store.dispatch(addVideo(video, this.props.token))
      this.next()
    })
  }

  next = () => {
    const { navigate } = this.props.navigation
    navigate('uploader')
  }

  cancelled = () => {
    if (Platform.OS === 'android') {
      ToastAndroid.show('Auswahl abgebrochen', ToastAndroid.SHORT)
    }
  }

  render() {
    return (
      <StartContainer>

        <VideoText />

        <VideoFooter onVideoButtonPress={this.onVideoButtonPress} />

      </StartContainer>
    )
  }

}

const mapStateToProps = ({ token }) => {
  return { token }
}

export const VideoSelector = connect(mapStateToProps)(VideoSelectorComponent)
