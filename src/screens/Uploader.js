import React from 'react'
import { Alert } from 'react-native'
import { connect } from 'react-redux'
import { createVideo, getUploadCrendentials, uploadVideo, uploadFinished } from '../api/apiClient'
import { updateVideoRecord, savePublicUri, saveRemoteId } from '../database/dbClient'
import RegularContainer from '../components/Containers/RegularContainer'
import VideoText from '../components/VideoUpload/VideoText'
import VideoFooter from '../components/VideoUpload/VideoFooter'
import RNFetchBlob from 'react-native-fetch-blob'


class UploaderComponent extends React.Component {
  state = {
    upload: false
  }

  showVideo = () => {
    const { navigate } = this.props.navigation
    navigate('videoPlayer')
  }

  onUploadProgress = ({ loaded, total}) => {
    console.log(`UPLOAD PROGRESS: ${loaded}/${total}`)

    this.setState({
      upload: {
        from: loaded,
        to: total,
      }
    })
  }

  // to be integrated with new api - shows the old usage
  createProject = async () => {
    const { video } = this.props

    let videoResponse, videoUploadResponse, uploadFinishedResponse

    console.log(video)

    try {
      videoResponse = await createVideo(this.props.token)
      console.log("VIDEO CREATE RESPONSE", videoResponse)

    } catch (error) {
      console.log(error)
      Alert.alert('Der Upload des Videos ist fehlgeschlagen, bitte versuchen Sie es später noch einmal.')

      return
    }

    try {
      videoUploadResponse = await uploadVideo(videoResponse.upload_url, this.props.video.uri, this.onUploadProgress)
      console.log("VIDEO UPLOADED RESPONSE", videoUploadResponse)

    } catch (error) {
      console.log("VIDEO UPLOAD ERROR")
      console.log(error)

      Alert.alert('Der Upload des Videos ist fehlgeschlagen, bitte versuchen Sie es später noch einmal.')

      return
    }

    try {
      uploadFinishedResponse = await uploadFinished(videoResponse.id, this.props.token)
      console.log("UPLOAD FINISHED RESPONSE", uploadFinishedResponse)

    } catch (error) {
      console.log("VIDEO FINISH ERROR")
      console.log(error)

      Alert.alert('Der Upload des Videos ist fehlgeschlagen, bitte versuchen Sie es später noch einmal.')

      return
    }

    try {
      await saveRemoteId(`${videoResponse.id}`, this.props.video)
    } catch (error) {
      console.log("REALM SAVE VIDEO RESPONSE ID ERROR")
      console.log(error)
    }

    this.setState({ upload: false })

    this.next()
  }

  next = async () => {
    const { navigation, video, token } = this.props

    await updateVideoRecord(video, {
      state: 'uploaded',
      code: token
    })

    console.log("UPLOADED VIDEO")
    console.log(video)

    navigation.navigate('process')
  }


  render() {
    return (
      <RegularContainer>

        <VideoText/>

        {/* TODO with api implemented: button with action sending video to aws */}
        <VideoFooter onVideoPlayPress={this.showVideo} onVideoUpload={this.createProject}/>

      </RegularContainer>
    )
  }

}

const mapStateToProps = ({ device, video, token }) => {
  return { device, video, token }
}

export const Uploader = connect(mapStateToProps)(UploaderComponent)
