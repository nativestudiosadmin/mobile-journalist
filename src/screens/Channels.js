import React from 'react'
import { connect } from 'react-redux'
import { NEXXTV_API } from '../config/api'
import { database } from '../config/database'
import { updateVideoRecord } from '../database/dbClient'
import { Video } from '../models/video.model'
import { globalStyles } from '../config/global-styles'
import { Alert, Image, ScrollView, View, Text } from 'react-native'
import { Button } from '../components/Button'
import Logo from '../components/Logo'

import RegularContainer from '../components/Containers/RegularContainer'
import ChannelsText from '../components/Channels/ChannelsText'
import ChannelsFooter from '../components/Channels/ChannelsFooter'

import { getChannels, publishVideo } from '../api/apiClient'


export class ChannelsComponent extends React.Component {
  state = {
    channels: [],
    selectedChannels: [],
  }

  componentDidMount() {
    this.getChannelsList()
  }

  next = () => {
    const { navigate } = this.props.navigation
    navigate('end')
  }

  getChannelsList = async () => {
    const { video } = this.props

    const channelsResponse = await getChannels(video.code)

    let newChannels = []

    for(const i in channelsResponse.channels) {
      channel = channelsResponse.channels[i]
      console.log(channel)
      newChannels.push(channel.platform)
    }

    this.setState({
      channels: channelsResponse.channels,
      selectedChannels: newChannels
    })

    // const body = new FormData()

    // body.append('cmd', 'getaccounts')
    // body.append('authkey', this.props.video.code)

    // return fetch(NEXXTV_API, {
    //   method: 'post',
    //   body,
    // }).then(response => response.json())
    //   .then(({ accounts }) => {
    //     const channels = accounts
    //     const selectedChannels = Object.keys(accounts)

    //     this.setState({ channels, selectedChannels })
    //   })
  }

  updateSelectedChannels = selectedChannels => {
    this.setState({selectedChannels})
  }

  publish = async () => {
    const { video } = this.props
    const { channels, selectedChannels } = this.state

    let channelIds = []
    for( const i in selectedChannels) {
      let channelName = selectedChannels[i]

      for ( const y in channels) {
        let channelObject = channels[y]

        console.log(channelObject, channelName)

        if (channelObject.platform == channelName) {
          channelIds.push(channelObject.id)
        }
      }
    }

    console.log(`Channel IDs ${channelIds}`)

    const publishResponse = await publishVideo(video.remoteId, channelIds.join(','), video.code)

    console.log("PUBLISH RESPONSE")
    console.log(publishResponse)

    // const body = new FormData()

    // body.append('cmd', 'publish')
    // body.append('authkey', video.code)
    // body.append('item', video.remoteId)
    // body.append('account', selectedChannels.join(','))

    // fetch(NEXXTV_API, {
    //   method: 'post',
    //   body
    // })
    //   .then(response => response.json())
    //   .then(response => {
    //     if (!response || response.result !== 'ok') {
    //       return Alert.alert(response.reason)
    //     }

    await updateVideoRecord(video, { state: "published" })
    this.next()
  }

  render() {
    return (
      <RegularContainer>

        <ChannelsText/>

        <ChannelsFooter
          selectedChannels={this.state.selectedChannels}
          updateSelectedChannels={this.updateSelectedChannels}
          publish={this.publish}/>

      </RegularContainer>
    )
  }
}

const mapStateToProps = ({ video }) => {
  return { video }
}

export const Channels = connect(mapStateToProps)(ChannelsComponent)
