import React from 'react'
import { Alert, ScrollView, Text, TouchableHighlight, View, Clipboard } from 'react-native'
import { connect } from 'react-redux'

import RegularContainer from '../components/Containers/RegularContainer'
import EditorModal from '../components/Editor/EditorModal'
import { getVideo, updateVideo } from '../api/apiClient'

class NameEditorComponent extends React.Component {
  state = {
    initialTitle: "",
    title: ""
  }

  // async componentDidMount() {
  componentDidMount() {
    this.fetchVideo()
    this.openModal()

    // try {
    //   await this.getSubtitles()
    // } catch (e) {
    //   Alert.alert(e.message)
    // }
  }

  fetchVideo = async () => {
    const { video } = this.props
    const videoResponse = await getVideo(video.remoteId, video.code)

    this.setState({
      title: videoResponse.title,
      initialTitle: videoResponse.title
    })

  }

  back = () => {
    this.props.navigation.goBack()
  }

  updateTitle = (title) => {
    console.log("TITLE UPDATE")
    console.log(title)

    this.setState({ title: title })
  }

  submitTitle = () => {
    const { video } = this.props

    console.log("TITLE SUBMIT")
    console.log(this.state.title)

    if (this.state.title != this.state.initialTitle) {
      console.log(`UPDATING!! ${this.state.title}`)
      updateVideo(video.remoteId, video.code, { title: this.state.title })
    }

    this.refs.modal.hide()
    this.back()
  }

  getSubtitles = async () => {
    // const { video } = this.propsrr
    // const response = await video.getCaptions()

    // if (!response) {
    //   throw new Error('Verbindung zum Server ist fehlgeschlagen. Bitte versuchen Sie es später noch einmal.')
    // }

    // if (response.result !== 'ok') {
    //   throw new Error(response.reason)
    // }

    // let subtitles = response.data.filter((caption) => caption.type === 'subtitles')

    // subtitles = subtitles.length > 0 ? subtitles[0].data : []

    // this.setState({ subtitles })
  }

  openModal = () => {
    this.refs.modal.show()
  }

  async onValueChange(value, finished) {
    // const { currentSubtitle } = this.state

    // currentSubtitle.caption = value

    // this.setState({ currentSubtitle })

    // if (finished) {
    //   this.refs.modal.hide()
    //   await this.saveSubtitles()
    // }
  }

  saveSubtitles = async () => {
    // const { video } = this.props
    // const { subtitles } = this.state
    // const body = new FormData()

    // body.append('cmd', 'setcaptions')
    // body.append('authkey', video.code)
    // body.append('item', video.remoteId)

    // for (const subtitle of subtitles) {
    //   body.append(`row-${subtitle.ID}`, subtitle.caption)
    // }

    // return fetch(NEXXTV_API, {
    //   method: 'post',
    //   body
    // }).then(response => response.json())
  }

  copy = () => {
    // const text = this.state.subtitles
    //   .map(subtitle => subtitle.caption)
    //   .join('\n')

    // Clipboard.setString(text)

    // Alert.alert('Untertitel kopiert!')
  }

  render() {
    // const subtitles = this.state.subtitles.map((subtitle) => {
    //   return (
    //     <TouchableHighlight key={subtitle.ID} onPress={this.openModal.bind(this, subtitle)}>
    //       <Text style={[globalStyles.text, { textAlign: 'left' }]}>{subtitle.caption}</Text>
    //     </TouchableHighlight>
    //   )
    // })

    return (
      <RegularContainer>

        <EditorModal ref="modal" value={this.state.title} onChange={this.updateTitle} onSubmit={this.submitTitle}/>

      </RegularContainer>

      // <ScrollView style={globalStyles.scrollView}>
      //   <View style={globalStyles.container}>
      //     <View style={globalStyles.textContainer}>
      //       <Text style={globalStyles.text}>
      //         Ändern Sie nachfolgenden die Untertitel:
      //                   </Text>
      //     </View>
      //     <View style={[globalStyles.textContainer, { marginTop: 20 }]}>
      //       {subtitles}
      //     </View>

      //     <Button onPress={this.next.bind(this)}
      //       size="small"
      //       style={{ marginTop: 20 }}>
      //       Speichern
      //               </Button>

      //     <Button onPress={this.copy.bind(this)}
      //       size="small"
      //       style={{ marginTop: 20 }}>
      //       Untertitel kopieren
      //               </Button>
      //   </View>

      //   <Modal ref="modal">
      //     <SimpleEditor title="Ändern Sie den Untertiel:"
      //       value={this.state.currentSubtitle.caption}
      //       multiline={true}
      //       onChange={this.onValueChange.bind(this)} />
      //   </Modal>
      // </ScrollView>
    )
  }
}

const mapStateToProps = ({ video }) => {
  return { video }
}

export const NameEditor = connect(mapStateToProps)(NameEditorComponent)
