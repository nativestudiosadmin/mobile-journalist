import { Start } from '../screens/Start'
import { VideoSelector } from '../screens/VideoSelector'
import { Editor } from '../screens/Editor'
import { End } from '../screens/End'
import { Uploader } from '../screens/Uploader'
import { VideoPlayer } from '../screens/VideoPlayer'
import { Process } from '../screens/Process'
import { Channels } from '../screens/Channels'
import { NameEditor } from '../screens/NameEditor'
import { SubtitlesEditor } from '../screens/SubtitlesEditor'
import { SubtitlesPlayer } from '../screens/SubtitlesPlayer'

export const routes = {
  'start': {
    screen: Start
  },
  'videoSelector': {
    screen: VideoSelector
  },
  'uploader': {
    screen: Uploader
  },
  'process': {
    screen: Process
  },
  'videoPlayer': {
    screen: VideoPlayer
  },
  'editor': {
    screen: Editor
  },
  'nameEditor': {
    screen: NameEditor
  },
  'subtitlesEditor': {
    screen: SubtitlesEditor
  },
  'subtitlesPlayer': {
    screen: SubtitlesPlayer
  },
  'channels': {
    screen: Channels
  },
  'end': {
    screen: End
  }
}
