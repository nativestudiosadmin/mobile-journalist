import Realm from 'realm';
import * as models from '../models';

const schema = Object.keys(models).map((key) => models[key]);
const schemaVersion = Math.max.call(...schema.map((s) => s.version));

export const database = new Realm({
    schema,
    schemaVersion,
    migration: (from, to) => {
        for (let model of schema) {
            console.log(model);
        }
    }
});

export function getCurrentVideo() {
  const videos = database.objects('Video')
      .filtered('state == "ready"')
      .map(video => video);

  const video = videos.length > 0 ? videos[videos.length - 1] : null;

  return video
}
