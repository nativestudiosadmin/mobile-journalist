import { StyleSheet } from 'react-native'

export const WIDTH_TARGET = 1080
export const BUTTON_FULL_WIDTH = 1.03
export const BUTTON_NARROW_WIDTH = 0.6
export const BUTTON_UNDERLAY_COLOR = '#ccc'
export const LOGO_HEIGHT = 72
export const DEFAULT_FONT_SIZE = 22
export const MAIN_FONT = "Lato-Light"

export const globalStyles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FFF',
    flex: 1,
    position: 'relative',
  },
  backgroundImageContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  backgroundImageNarrow: {
    flex: 1,
    width: '85%',
    resizeMode: 'stretch',
    alignSelf: 'flex-start',
  },
  backgroundImageHalf: {
    flex: 0.52,
    width: '100%',
    resizeMode: 'stretch',
    alignSelf: 'flex-start',
  },
  scrollViewContainer: {
    flexGrow: 1,
    justifyContent: 'space-between',
  },
  narrowView: {
    paddingLeft: '9%',
    paddingRight: '31%',
  },
  centeredView: {
    paddingLeft: '20%',
    paddingRight: '20%',
  },
  logoContainer: {
    alignSelf: 'flex-end',
  },
  textContainer: {
  },
  actionContainer: {
    borderWidth: 10,
    borderColor: '#173e7d',
    borderRadius: 20,
    marginTop: 10,
    justifyContent: 'center',
    paddingTop: 30,
    paddingBottom: 30,
  },
  text: {
    fontFamily: MAIN_FONT,
    backgroundColor: 'transparent',
    fontSize: DEFAULT_FONT_SIZE,
    lineHeight: 24,
    color: '#000',
    textAlign: 'left',
  },
  footerContainer: {
  },
  footerTextFlow: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '140%',
    marginLeft: '-20%'
,  },
  footerText: {
    backgroundColor: 'transparent',
    fontSize: 10,
    marginTop: 7,
    marginBottom: 7,
    paddingLeft: 4,
  },
  videoText: {
    textAlign: 'left',
    fontFamily: 'TitilliumWeb-Thin',
    backgroundColor: 'transparent',
    fontSize: 20,
    color: '#000',
  },
  videoButton: {
    width: '100%',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 30,
    borderRadius: 40,
  },
  optionButton: {
    width: '100%',
    marginTop: 10,
    marginLeft: 10,
    borderRadius: 40,
  },
  modal: {
    backgroundColor: '#000',
    width: '73%',
    marginTop: 70,
    marginLeft: '7%',
    paddingTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 5,
    elevation: 30,
  },
  modalInput: {
    fontFamily: MAIN_FONT,
    backgroundColor: 'transparent',
    width: '100%',
    textAlign: 'left',
    fontSize: DEFAULT_FONT_SIZE,
    lineHeight: 23,
    color: '#FFF',
  },
})
