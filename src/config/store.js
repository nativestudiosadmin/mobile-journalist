import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { device } from '../reducers/device.reducer'
import { token } from '../reducers/token.reducer'
import { video } from '../reducers/video.reducer'

const reducers = combineReducers({
  device,
  token,
  video,
})

const configureStore = function () {
  let createStoreMiddleware

  if (__DEV__) {
    const loggerMiddleware = createLogger()
    createStoreMiddleware = applyMiddleware(thunk, loggerMiddleware)(createStore)
  } else {
    createStoreMiddleware = applyMiddleware(thunk)(createStore)
  }

  return createStoreMiddleware(reducers)
}

export const store = configureStore()
