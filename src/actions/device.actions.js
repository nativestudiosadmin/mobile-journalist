import DeviceInfo from 'react-native-device-info';
import { CREACE_API } from '../config/api';

export const DEVICE_UPDATE = 'DEVICE_UPDATE';

function updateDevice(json) {
    return {
        type: DEVICE_UPDATE,
        device: json,
    }
}

export function registerDevice(token = null) {
    const device = {
        id: DeviceInfo.getUniqueID(),
        manufacturer: DeviceInfo.getManufacturer(),
        model: DeviceInfo.getModel(),
        system_name: DeviceInfo.getSystemName(),
        system_version: DeviceInfo.getSystemVersion(),
        bundle_id: DeviceInfo.getBundleId(),
        version: DeviceInfo.getReadableVersion(),
        locale: DeviceInfo.getDeviceLocale(),
        country: DeviceInfo.getDeviceCountry(),
    };

    if (token !== null) {
        device.push_token = token;
    }

    return (dispatch) => {
        return fetch(CREACE_API + '/devices', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(device)
        })
            .then(response => response.json())
            .then(json => dispatch(updateDevice(Object.assign(device, json))));
    };
}
