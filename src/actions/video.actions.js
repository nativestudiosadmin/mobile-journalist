import { Video } from '../models/video.model'
import { database } from '../config/database'

export const VIDEO_SET = 'VIDEO_SET'

export function addVideo(video, code) {
  return (dispatch) => {
    return new Promise((resolve) => {
      database.write(() => {
        const date = new Date()

        date.setTime(Date.now())

        const data = {
          id: Date.now(),
          remoteId: null,
          uri: video.uri,
          publicUri: null,
          code: code,
          state: 'new',
          title: null,
          description: null,
          date: date,
        }

        const entity = database.create(Video, data)

        dispatch(setVideo(entity))

        resolve(video)
      })
    })
  }
}

export function setVideo(video) {
  return {
    type: VIDEO_SET,
    video,
  }
}
