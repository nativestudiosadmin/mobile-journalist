export const TOKEN_UPDATE = 'TOKEN_UPDATE'

export function updateToken(token) {
  return {
    type: TOKEN_UPDATE,
    token,
  }
}
