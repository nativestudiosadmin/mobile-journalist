import { database } from '../config/database'
import { Video } from '../models/video.model'

export async function savePublicUri(uri, video) {
  return new Promise((resolve) => {
    database.write(() => {
      video.state = 'uploaded'
      video.publicUri = uri

      database.create(Video, video, true)

      resolve()
    })
  })
}

export async function saveRemoteId(remoteId, video) {
  return new Promise((resolve) => {
    database.write(() => {
      video.remoteId = remoteId

      database.create(Video, video, true)

      resolve()
    })
  })
}

export async function saveVideoInfo(video, status) {
  return new Promise((resolve) => {
    database.write(() => {
      video.state = status
      //video.title = response.title
      //video.description = response.description

      database.create(Video, video, true)

      resolve()
    })
  })
}

export async function updateVideoRecord(video, args = {}) {
  return new Promise((resolve) => {
    database.write(() => {

      for ( const key in args) {
        video[key] = args[key]
      }

      database.create(Video, video, true)

      resolve()
    })
  })
}

