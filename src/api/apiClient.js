import { CREACE_API, API_URL } from '../config/api'
import RNFetchBlob from 'react-native-fetch-blob'

const fileFetch = (url, opts = {}, onProgress) => {
  return new Promise( (res, rej) => {
    var xhr = new XMLHttpRequest()
    xhr.open(opts.method || 'get', url)
    for (var k in opts.headers || {})
        xhr.setRequestHeader(k, opts.headers[k])
    xhr.onload = e => res(e.target)
    xhr.onerror = rej
    if (xhr.upload && onProgress)
        xhr.upload.onprogress = onProgress
    xhr.send(opts.body);
  })
}

export async function authRequest(code) {
  console.log("Auth request")
  console.log(API_URL, code)

  return fetch(API_URL + '/sessions', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      code: code
    }),
  }).then((response) => {
    console.log("AUTH RESPONSE")
    console.log(response)
    return response
  }).then(response => response.json()
         ).catch((err) => {
           console.log('Request Error')
           console.log(err)
         })
}

// 3 UPLOADER SCREEN API METHODS

// to be integrated with new api
export async function getUploadCrendentials(device) {
  return fetch(`${CREACE_API}/files`, {
    method: 'POST',
    headers: {
      'Authorization': 'Bearer ' + device.token,
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      type: 'video',
      filename: 'video.mov'
    })
  }).then(response => response.json())
}

// {
//   "id":1,
//   "title" : "Test Video",
//   "status":"initiated",
//   "upload_url":"https://mobile-journalist.s3.amazonaws.com/videos/1_test_video?X-Amz-Expires=43200\u0026X-Amz-Date=20180703T153808Z\u0026X-Amz-Algorithm=AWS4-HMAC-SHA256\u0026X-Amz-Credential=AKIAJ6CHCFUWM6UJZKBQ/20180703/us-east-1/s3/aws4_request\u0026X-Amz-SignedHeaders=host\u0026X-Amz-Signature=243d09a7e683e9af372d0ff9e59fe16e6b2042a2dbb004d8490701ad8ad72011"
// }"
// to be integrated with new API
export async function createVideo(token) {
  console.log(`Access Token: ${token}`)

  return fetch(API_URL + '/videos', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'AuthorizationToken': token,
    },
    body: JSON.stringify({
      title: "video.mp4"
    }),
  }).then((response) => {
    console.log(response)
    return response
  }).then(response => response.json())

}

export async function uploadVideo(uploadUrl, videoUri, onProgress) {

  let data = {
    uri: videoUri,
    type: 'application/octet-stream',
    name: 'video.mov',
  }

  return fileFetch(uploadUrl, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/octet-stream',
    },
    body: data,
  }, onProgress).then((response) => {
    console.log("FILE FETCH RESPONSE")
    console.log(response)

    return response
  })
}

export async function uploadFinished(videoId, token) {
  console.log(`Access Token: ${token}`)

  return fetch(API_URL + `/videos/${videoId}/upload_finished`, {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'AuthorizationToken': token,
    }
  }).then((response) => {
    console.log(response)
    return response
  }).then(response => response.json())

}

export async function getVideo(videoId, token) {
  return fetch(API_URL + `/videos/${videoId}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'AuthorizationToken': token,
    }
  }).then((response) => {
    console.log(`GET VIDEO RESPONSE (status: ${response.status})`)
    console.log(response)
    return response
  }).then(response => response.json())

}

export async function getChannels(token) {
  return fetch(API_URL + `/channels`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'AuthorizationToken': token,
    }
  }).then((response) => {
    console.log("GET CHANNELS RESPONSE")
    console.log(response)
    return response
  }).then(response => response.json())

}

export async function updateVideo(videoId, token, params = {}) {
  return fetch(API_URL + `/videos/${videoId}`, {
    method: 'PATCH',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'AuthorizationToken': token,
    },
    body: JSON.stringify(params)
  }).then((response) => {
    console.log("VIDEO UPDATED")
    console.log(response)
    return response
  }).then(response => response.json())

}

export async function publishVideo(videoId, channelId, token) {
  console.log(`Access Token: ${token}`)

  return fetch(API_URL + `/videos/${videoId}/publish`, {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'AuthorizationToken': token,
    },
    body: JSON.stringify({ channel_id: channelId })
  }).then((response) => {
    console.log(response)
    return response
  }).then(response => response.json())

}


