import { TOKEN_UPDATE } from '../actions/token.actions'
import { database, getCurrentVideo } from '../config/database'

const initialState = ''

const video = getCurrentVideo()

if (video) {
  initialState = video.code
}

export const token = (state = initialState, action) => {
  switch (action.type) {
    case TOKEN_UPDATE:
      return action.token
    default:
      return state
  }
}
