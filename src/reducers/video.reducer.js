import { VIDEO_SET } from '../actions/video.actions'
import { database, getCurrentVideo } from '../config/database'

const currentVideo = getCurrentVideo()
const initialState = currentVideo ? currentVideo : null

export const video = (state = initialState, action) => {
  switch (action.type) {
    case VIDEO_SET:
      return action.video
    default:
      return state
  }
}
