import { DEVICE_UPDATE } from '../actions/device.actions';

const initialState = {};

export const device = (state = initialState, action) => {
    switch (action.type) {
        case DEVICE_UPDATE:
            return action.device;
        default:
            return state;
    }
};
