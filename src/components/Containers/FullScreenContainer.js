import React from 'react'
import { ScrollView, View, Image } from 'react-native'
import { globalStyles } from '../../config/global-styles'
import BackgroundImage from '../../assets/background1.jpg'


export default class FullScreenContainer extends React.Component {
  render() {
    return (
      <View style={globalStyles.mainContainer}>

        <View style={[globalStyles.backgroundImageContainer, {zIndex: 0}]}>
          <Image style={{flex: 1}} source={BackgroundImage} fadeDuration={0}/>
        </View>

        <ScrollView contentContainerStyle={globalStyles.scrollViewContainer}>

          {this.props.children}

        </ScrollView>

      </View>
    )
  }
}