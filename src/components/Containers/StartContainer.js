import React from 'react'
import { ScrollView, View, Image } from 'react-native'
import Logo from '../Logo'
import BackgroundImage from '../../assets/background1.jpg'
import { globalStyles, LOGO_HEIGHT } from '../../config/global-styles'


export default class NarrowContainer extends React.Component {
  render() {

    return (
      <View style={globalStyles.mainContainer}>

        <View style={globalStyles.backgroundImageContainer}>
          <Image style={globalStyles.backgroundImageNarrow} source={BackgroundImage} fadeDuration={0}/>
        </View>

        <ScrollView contentContainerStyle={[globalStyles.scrollViewContainer, globalStyles.narrowView]}>
          <View style={[globalStyles.logoContainer, {paddingTop: 60}]}>
            <Logo height={LOGO_HEIGHT} />
          </View>

          {this.props.children}

        </ScrollView>

      </View>
    )
  }
}
