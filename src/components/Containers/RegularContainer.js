import React from 'react'
import { ScrollView, View, Image } from 'react-native'
import BackgroundImage from '../../assets/background1.jpg'
import { globalStyles } from '../../config/global-styles'


export default class RegularContainer extends React.Component {
  render() {

    return (
      <View style={globalStyles.mainContainer}>

        <View style={globalStyles.backgroundImageContainer}>
          <Image style={globalStyles.backgroundImageHalf} source={BackgroundImage} fadeDuration={0}/>
        </View>

        <ScrollView contentContainerStyle={globalStyles.scrollViewContainer}>

          {this.props.children}

        </ScrollView>

      </View>
    )
  }
}
