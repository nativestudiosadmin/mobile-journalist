import React from 'react'
import { View, Text } from 'react-native'
import { globalStyles } from '../../config/global-styles'


export default class PushText extends React.Component {
  render() {
    return (
      <View style={[globalStyles.narrowView, {height: '83%', width: '100%', justifyContent: 'flex-end'}]}>
        <Text style={[globalStyles.text, {marginBottom: 10}]}>
          Dein Video wird jetzt bearbeitet.
        </Text>
        <Text style={[globalStyles.text, {marginBottom: 10}]}>
          Sobald der Vorgang abgeschlossen ist, schicken wir Dir eine Nachricht.
        </Text>
        <Text style={[globalStyles.text, { paddingBottom: 5 }]}>
          Du kannst dann die Untertitel, die Video- Beschreibung und den Video-Namen bearbeiten und das Video freigeben.
        </Text>
      </View> 
    )
  }
}