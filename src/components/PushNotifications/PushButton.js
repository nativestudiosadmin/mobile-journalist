import React from 'react'
import { View, TouchableHighlight, Image, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_FULL_WIDTH, BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import PushImage from '../../assets/push.png'


export default class PushButton extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_FULL_WIDTH
    const { width, height } = Image.resolveAssetSource(PushImage)
    this.imageHeight = this.imageWidth / width * height
  }

  render() {
    return (
      <View style={{height: '17%', width: '100%'}}>
        <TouchableHighlight 
          style={[globalStyles.optionButton, {marginTop: 15}]} 
          underlayColor={BUTTON_UNDERLAY_COLOR}
          onPress={this.props.onEnableNotifications}>

          <Image source={PushImage} style={{height: this.imageHeight, width: this.imageWidth}}/>
        </TouchableHighlight>
      </View>
    )
  }
}

PushButton.propTypes = {
  onEnableNotifications: PropTypes.func.isRequired
}