import React from 'react'
import PropTypes from 'prop-types'
import { Image } from 'react-native'
import LogoImage from '../assets/logo.png'


class Logo extends React.Component {
  constructor(props) {
    super(props)

    const { width, height } = Image.resolveAssetSource(LogoImage)
    this.width = this.props.height / height * width
  }

  render() {
    return (
      <Image style={{ width: this.width, height: this.props.height }} source={LogoImage} />
    )
  }
}

Logo.propTypes = {
  height: PropTypes.number,
}

export default Logo