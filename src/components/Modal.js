import React from 'react';
import { Modal as RNModal, StyleSheet, View } from 'react-native';

export class Modal extends React.Component {
    state = {
        visible: false,
    };

    render() {
        return (
            <RNModal animationType="slide"
                   visible={this.state.visible}
                   transparent={true}
                   ref="modal"
                   onRequestClose={this.hide.bind(this)}>
                <View style={[styles.container, this.props.style || {}]}>
                    {this.props.children}
                </View>
            </RNModal>
        );
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        padding: 20,
    },
});
