import React from 'react'
import { Text, View, Image, TouchableHighlight, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_NARROW_WIDTH, BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import ArrowImage from '../../assets/arrow.png'

export default class VideoFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_NARROW_WIDTH
    const { width, height } = Image.resolveAssetSource(ArrowImage)
    this.imageHeight = this.imageWidth / width * height
  }

  render() {
    return (
      <View style={globalStyles.footerContainer}>

        <Text style={[globalStyles.videoText, {textAlign: 'center', width: '120%', marginLeft: '-11%'}]}>
          VIDEO VOM ALBUM LADEN
        </Text>

        <TouchableHighlight style={globalStyles.videoButton} onPress={this.props.onVideoButtonPress} underlayColor={BUTTON_UNDERLAY_COLOR}>
          <Image source={ArrowImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
        </TouchableHighlight>

      </View>
    )
  }
}

VideoFooter.propTypes = {
  onVideoButtonPress: PropTypes.func.isRequired
}