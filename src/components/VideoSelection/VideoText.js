import React from 'react'
import { Text, View } from 'react-native'
import { globalStyles } from '../../config/global-styles'


export default class VideoText extends React.Component {
  render() {
    return (
      <View style={globalStyles.textContainer}>

        <Text style={globalStyles.text}>
          Willkommen.
        </Text>
        <Text style={[globalStyles.text, { paddingBottom: 5 }]}>
          Du kannst jetzt Dein Video aus dem Album auswählen und in die App laden.
        </Text>

      </View>
    )
  }
}