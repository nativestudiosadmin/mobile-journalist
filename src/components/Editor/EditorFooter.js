import React from 'react'
import { View, Image, TouchableHighlight, Text, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_FULL_WIDTH, BUTTON_UNDERLAY_COLOR, MAIN_FONT, DEFAULT_FONT_SIZE } from '../../config/global-styles'
import SubtitleImage from '../../assets/subtitle.png'
import KeywordImage from '../../assets/keyword.png'
import NameImage from '../../assets/name.png'
import SubmitImage from '../../assets/submit.png'


export default class EditorFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_FULL_WIDTH
    const { width, height } = Image.resolveAssetSource(SubtitleImage)
    this.imageHeight = this.imageWidth / width * height
  }

  openModal = (modalFunc) => {
    this.setState({infoShows: false})
    modalFunc()
  }


  render() {
    return (
      <View style={{flex: 1}}>
        <View style={[globalStyles.modal, {height: 110, flexDirection: 'column'}]}>
          <View style={{height: '80%', width: '100%', paddingTop: 15, paddingBottom: 20}}>

            <Text style={{color: '#FFF', fontFamily: MAIN_FONT, fontSize: DEFAULT_FONT_SIZE, backgroundColor: 'transparent'}}>
              Du kannst jetzt die Metadaten bearbeiten.
            </Text>

          </View>
        </View>       

        <View style={[globalStyles.footerContainer, { height: '48%', position: 'absolute', bottom: 0}]}>

          <View>
            <TouchableHighlight
              style={globalStyles.optionButton} 
              onPress={this.props.openSubtitlesEditor} 
              underlayColor={BUTTON_UNDERLAY_COLOR}>

              <Image source={SubtitleImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
            </TouchableHighlight>
          </View>
          
          <View>
            <TouchableHighlight
              style={globalStyles.optionButton} 
              onPress={this.props.openDescriptionModal} 
              underlayColor={BUTTON_UNDERLAY_COLOR}>

              <Image source={KeywordImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
            </TouchableHighlight>
          </View>

          <View>
            <TouchableHighlight
              style={globalStyles.optionButton} 
              onPress={this.props.openNameModal} 
              underlayColor={BUTTON_UNDERLAY_COLOR}>

              <Image source={NameImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
            </TouchableHighlight>
          </View>

          <View>
            <TouchableHighlight
              style={globalStyles.optionButton} 
              onPress={this.props.goToChannels} 
              underlayColor={BUTTON_UNDERLAY_COLOR}>

              <Image source={SubmitImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
            </TouchableHighlight>
          </View>
        
        </View>

      </View>
    )
  }
}

EditorFooter.propTypes = {
  openSubtitlesEditor: PropTypes.func.isRequired,
  openDescriptionModal: PropTypes.func.isRequired,
  openNameModal: PropTypes.func.isRequired,
  goToChannels: PropTypes.func.isRequired
}