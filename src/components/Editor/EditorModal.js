import React from 'react'
import { TouchableHighlight, Modal, View, TextInput, Image } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import AbortImage from '../../assets/abort-small.png'
import ContinueImage from '../../assets/continue-small.png'

export default class EditorModal extends React.Component {
  state = {
    visible: false,
    value: ''
  }

  show = () => {
    this.setState({ visible: true })
  }

  hide = () => {
    this.setState({ visible: false })
  }

  onHandleReturn = () => {
    this.props.onSubmit()
  }

  onChangeText = value => {
    this.props.onChange(value)
  }

  render() {
    return (
      <Modal animationType="slide"
        visible={this.state.visible}
        transparent={true}
        ref="modal"
        onRequestClose={this.hide}>

        <View style={[globalStyles.modal, {height: 300, flexDirection: 'column', marginTop: 30}]}>
          <View style={{height: '80%', width: '100%', paddingTop: 15, paddingBottom: 20}}>

            <TextInput style={globalStyles.modalInput}
              value={this.props.value}
              caretHidden={false}
              multiline={false}
              underlineColorAndroid="rgba(0,0,0,0)"
              autoFocus={true}
              onSubmitEditing={this.onHandleReturn}
              onChangeText={this.onChangeText} />

          </View>

          <View style={{height: '17%', width: '136%', flexDirection: 'row', justifyContent: 'space-around', marginLeft: '-15%'}}>

          { false && (
            <View>
            <TouchableHighlight
                style={{height: '70%', borderRadius: 40}}
                underlayColor={BUTTON_UNDERLAY_COLOR}
                onPress={this.hide}>
              <Image source={AbortImage}
                style={{height: '100%'}}
                resizeMode="contain"
                onPress={this.hide}/>
            </TouchableHighlight>

            <TouchableHighlight
                style={{height: '70%', borderRadius: 40}}
                underlayColor={BUTTON_UNDERLAY_COLOR}
                onPress={this.hide}>
              <Image source={ContinueImage}
                style={{height: '100%'}}
                resizeMode="contain"
                onPress={this.hide}/>
            </TouchableHighlight>
            </View>
          )}

          </View>
        </View>

      </Modal>
    )
  }
}

EditorModal.propTypes = {
  // onSubmit: PropTypes.func.isRequired
}
