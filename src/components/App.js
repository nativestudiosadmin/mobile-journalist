import React from 'react';
import { StackNavigator } from 'react-navigation';
import { routes } from '../config/routes';
import { store } from '../config/store';
import { registerDevice } from '../actions/device.actions';
import { Provider } from 'react-redux';
import { database, getCurrentVideo } from '../config/database';
import { updateVideoRecord } from '../database/dbClient';

const video = getCurrentVideo()

console.log("CURRENT VIDEO")
console.log(video)

let initialRouteName = 'start'

if (video && video.remoteId && video.remoteId.length > 0 && video.code && video.code.length > 0) {

  if (video.state === 'uploaded') {
    initialRouteName = 'process'
  }

  if (video.state === 'ready') {
    initialRouteName = 'editor'
  }
}

const options = {
    headerMode: 'none',
    initialRouteName,
};

const AppNavigator = StackNavigator(routes, options);

export class App extends React.Component {
    componentWillMount() {
        const { dispatch } = store;

        dispatch(registerDevice());
    }

    render() {
        return (
            <Provider store={store}>
                <AppNavigator/>
            </Provider>
        );
    }
}
