import React from 'react'
import { TextInput } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles } from '../../config/global-styles';


export default class CodeInput extends React.Component {
  state = {
    value: ''
  }

  onHandleSubmit = () => {
    this.props.onCodeSubmit(this.state.value)
  }

  onChangeText = value => {
    this.setState({value})
  }

  render() {
    return (
      <TextInput style={globalStyles.modalInput} 
        value={this.state.value}
        caretHidden={true}
        underlineColorAndroid="#FFF"
        autoFocus={true}
        onSubmitEditing={this.onHandleSubmit}
        onChangeText={this.onChangeText} />
    )
  }
}

CodeInput.propTypes = {
  onCodeSubmit: PropTypes.func.isRequired
}