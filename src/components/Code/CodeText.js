import React from 'react'
import { Text, View } from 'react-native'
import { globalStyles } from '../../config/global-styles'


export default class CodeFooter extends React.Component {
  render() {
    return (
      <View style={globalStyles.textContainer}>

        <Text style={globalStyles.text}>
          Willkommen.
        </Text>
        <Text style={globalStyles.text}>
          Bitte gib unten Deinen individuellen Code ein.
        </Text>
        <Text style={[globalStyles.text, { marginTop: 15, paddingBottom: 5 }]}>
          Du kannst danach Dein Video hochladen und die Untertitel bearbeiten und freigeben.
        </Text>

      </View>
    )
  }
}