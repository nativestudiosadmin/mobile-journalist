import React from 'react'
import { Text, View, Linking, TouchableHighlight, Dimensions, Image } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_UNDERLAY_COLOR, BUTTON_NARROW_WIDTH } from '../../config/global-styles'
import CodeImage from '../../assets/code.png'


export default class CodeFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_NARROW_WIDTH
    const { width, height } = Image.resolveAssetSource(CodeImage)
    this.imageHeight = this.imageWidth / width * height
  }

  render() {
    return (
      <View style={globalStyles.footerContainer}>

        <TouchableHighlight style={[globalStyles.videoButton, {marginBottom: 0}]} onPress={this.props.onCodeButtonPress} underlayColor={BUTTON_UNDERLAY_COLOR}>
          <View>
            <Image source={CodeImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
          </View>
        </TouchableHighlight>

        <View style={globalStyles.footerTextFlow}>
          <Text style={[globalStyles.footerText, { paddingLeft: 0 }]}>
            nexxGO is a service of
          </Text>
          <Text style={globalStyles.footerText} onPress={() => Linking.openURL('https://nexx.tv/')}>
            nexx.TV
          </Text>
          <Text style={globalStyles.footerText}>
            and
          </Text>
          <Text style={globalStyles.footerText} onPress={() => Linking.openURL('https://www.native-studios.com/mobilejournalist')}>
            Native Studios
          </Text>
        </View>

      </View>
    )
  }
}

CodeFooter.propTypes = {
  onCodeButtonPress: PropTypes.func.isRequired
}