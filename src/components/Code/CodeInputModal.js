import React from 'react'
import { ActivityIndicator, Alert, Animated, Modal, Text, View } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles } from '../../config/global-styles'
import CodeInput from './CodeInput'

export default class CodeInputModal extends React.Component {
  state = {
    visible: false,
    isLoading: false,
    left: new Animated.Value(0),
  }

  animateWrongCode = () => {
    Animated.sequence([
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: -10
        }
      ),
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: 10
        }
      ),
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: -10
        }
      ),
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: 10
        }
      ),
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: -10
        }
      ),
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: 10
        }
      ),
      Animated.timing(
        this.state.left, {
          duration: 50,
          toValue: 0
        }
      ),
    ]).start()
  }

  onCodeSubmit = async code => {
    this.setState({ isLoading: true })

    const response = await this.props.onCheckCode(code)

    this.setState({ isLoading: false })

    if (!response) { return Alert.alert('Verbindung zum Server ist fehlgeschlagen.', 'Bitte versuchen Sie es später noch einmal.') }

    if (!response['token']) { return this.animateWrongCode() }

    this.props.onCodeSubmit(response)
  }

  show = () => {
    this.setState({ visible: true })
  }

  hide = () => {
    this.setState({ visible: false })
  }


  render() {
    const content = this.state.isLoading ?
      <ActivityIndicator size="large" style={{marginTop: 7}}/> :
      <CodeInput onCodeSubmit={this.onCodeSubmit} />

    return (
      <Modal animationType="slide"
        visible={this.state.visible}
        transparent={true}
        ref="modal"
        onRequestClose={this.hide}>
        <View style={[globalStyles.modal, {height: 90}]}>
          <Animated.View style={{ flex: 1, left: this.state.left }}>
            <Text style={{fontSize: 13, color: '#fff'}}>Please enter Your code and hit ENTER</Text>
            <View style={{flex: 1}}>
              {content}
            </View>
          </Animated.View>
        </View>
      </Modal>
    )
  }
}

CodeInputModal.propTypes = {
  onCheckCode: PropTypes.func.isRequired,
  onCodeSubmit: PropTypes.func.isRequired
}
