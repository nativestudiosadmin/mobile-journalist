import React from 'react'
import { View, Image, TouchableHighlight, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_FULL_WIDTH, BUTTON_UNDERLAY_COLOR, LOGO_HEIGHT } from '../../config/global-styles'
import HomeImage from '../../assets/home.png'
import Logo from '../Logo'


export default class EndFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_FULL_WIDTH
    const { width, height } = Image.resolveAssetSource(HomeImage)
    this.imageHeight = this.imageWidth / width * height
  }

  render() {
    return (
      <View style={[globalStyles.footerContainer, { height: '48%', position: 'absolute', bottom: 0, flexDirection: 'column'}]}>

        <View>
          <TouchableHighlight
            style={globalStyles.optionButton} 
            onPress={this.props.goToStart} 
            underlayColor={BUTTON_UNDERLAY_COLOR}>

            <Image source={HomeImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
          </TouchableHighlight>
        </View>

        <View style={{position: 'absolute', bottom: 40, right: 60}}>
          <Logo height={LOGO_HEIGHT}/>
        </View>
        
      </View>
    )
  }
}

EndFooter.propTypes = {
  goToStart: PropTypes.func.isRequired,
}