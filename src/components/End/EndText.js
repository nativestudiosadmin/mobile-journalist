import React from 'react'
import { Text, View } from 'react-native'
import { globalStyles } from '../../config/global-styles'


export default class EndText extends React.Component {
  render() {
    return (
      <View style={[globalStyles.narrowView, { height: '52%', justifyContent: 'center', paddingTop: 20 }]}>
        <Text style={[globalStyles.text, { paddingBottom: 5 }]}>
          Vielen Dank für Deinen Beitrag. Wenn Du jetzt einen weiteren Beitrag bearbeiten möchtest, gehe bitte zurück zur Homepage.
        </Text>
      </View>
    )
  }
}