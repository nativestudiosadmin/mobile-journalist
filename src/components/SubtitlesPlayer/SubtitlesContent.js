import React from 'react'
import Video from 'react-native-video'
import { View, Text, Image, TouchableHighlight } from 'react-native'
import PropTypes from 'prop-types'
import { MAIN_FONT} from '../../config/global-styles'
import PlayImage from '../../assets/playVideo.png'

export default class SubtitlesContent extends React.Component {
  state = {
    paused: true
  }

  togglePlay = () => {
    this.setState({paused: !this.state.paused})
  }

  render() {
    let playImage = !this.state.paused ? <View/> : <Image source={PlayImage} style={{width: 50, height: 50}} />

    return (
      <View style={{height: '74%', width: '90%', backgroundColor: '#000', position: 'absolute', top: 0, left: "5%", top: '6%'}} >
        <Video
          style={{height: '40%', width: "100%"}}
          resizeMode="contain"
          paused={this.state.paused}
          onEnd={this.togglePlay} 
          source={{uri: this.props.videoUri}}/>

        <TouchableHighlight
          style={{ position: 'absolute', width: 40, height: '40%', width: '100%', alignItems: 'center', justifyContent: 'center'}} 
          onPress={this.togglePlay} 
          underlayColor={'rgba(0,0,0,0.1)'}>

          {playImage}
        </TouchableHighlight>
        
        <View style={{height: '60%', width: '100%', paddingVertical: 20, paddingHorizontal: 12}}>
          <Text style={{color: '#FFF', fontFamily: MAIN_FONT, fontSize: 20, backgroundColor: 'transparent'}}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reprehenderit distinctio repellendus non pariatur aut necessitatibus sed dicta inventore quasi? Impedit dolore quod laborum, omnis reprehenderit aliquam veritatis voluptatem molestiae!
          </Text>
        </View>

      </View>
    )
  }
}

SubtitlesContent.propTypes = {
  videoUri: PropTypes.string.isRequired
}