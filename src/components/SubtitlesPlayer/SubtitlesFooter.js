import React from 'react'
import { View, TouchableHighlight, Image, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import BackImage from '../../assets/back.png'
import NextImage from '../../assets/next.png'


export default class SubtitlesFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width / 2.5
    const { width, height } = Image.resolveAssetSource(BackImage)
    this.imageHeight = this.imageWidth / width * height
  }

  render() {
    return (
      <View style={{height: '20%', width: '100%', position: 'absolute', bottom: 0, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}} >
        <TouchableHighlight 
          style={{width: this.imageWidth, height: this.imageHeight, borderRadius: 40}} 
          underlayColor={BUTTON_UNDERLAY_COLOR}
          onPress={this.props.onBackClick}>

          <Image source={BackImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
        </TouchableHighlight>

        <TouchableHighlight 
          style={{width: this.imageWidth, height: this.imageHeight, borderRadius: 40}} 
          underlayColor={BUTTON_UNDERLAY_COLOR}
          onPress={this.props.onNextClick}>

          <Image source={NextImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
        </TouchableHighlight>
      </View>
    )
  }
}

SubtitlesFooter.propTypes = {
  onBackClick: PropTypes.func.isRequired,
  onNextClick: PropTypes.func.isRequired
}