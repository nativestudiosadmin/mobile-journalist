import React from 'react';
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native';
import { COLOR_PRIMARY } from '../config/colors';

export class Button extends React.Component {
    render() {
        const inner = typeof this.props.children === 'string' ?
            <Text style={[styles.text, this.props.textStyle]}>{this.props.children}</Text> :
            <View style={[{}, this.props.innerStyle]}>{this.props.children}</View>;

        return (
            <TouchableHighlight
                style={[this.props.size === 'small' ? styles.containerSmall : styles.container, this.props.style]}
                onPress={this.props.onPress}>
                <View>
                    {inner}
                </View>
            </TouchableHighlight>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderColor: COLOR_PRIMARY,
        borderRadius: 20,
        borderWidth: 10,
        padding: 30,
    },
    containerSmall: {
        borderColor: COLOR_PRIMARY,
        borderRadius: 20,
        borderWidth: 10,
        padding: 10,
    },
    text: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 30,
    }
});
