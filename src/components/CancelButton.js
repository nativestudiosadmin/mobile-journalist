import React from 'react';
import { Alert } from 'react-native';
import { Button } from './Button';
import { connect } from 'react-redux';
import { database } from '../config/database';
import { Video } from '../models/video.model';

class CancelButtonComponent extends React.Component {
    render() {
        return <Button style={{ marginTop: 20 }} size="small" onPress={this.onPress.bind(this)}>Abbrechen</Button>
    }

    onPress() {
        const { video, navigation } = this.props;
        const { navigate } = navigation;

        Alert.alert(
            'Wirklich abbrechen?',
            'Wenn Sie abbrechen, wird der Prozess beendet und Sie gelangen zur Startseite zurück.',
            [{
                text: 'Nein'
            }, {
                text: 'Ja',
                onPress: () => {
                    this.props.onPress && this.props.onPress();

                    database.write(() => {
                        video.state = 'cancelled';
                        database.create(Video, video, true);
                    });

                    navigate('start');
                }
            }]
        );
    }
}

const mapStateToProps = ({ video }) => {
    return { video };
};

export const CancelButton = connect(mapStateToProps)(CancelButtonComponent);
