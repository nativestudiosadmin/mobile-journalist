import React from 'react'
import { Text, View } from 'react-native'
import { globalStyles } from '../../config/global-styles'


export default class VideoText extends React.Component {
  render() {
    return (
      <View style={[globalStyles.narrowView, { height: '52%', justifyContent: 'flex-end' }]}>
        <Text style={[globalStyles.text, { paddingRight: '12%', paddingBottom: 30 }]}>
          Du kannst Dein Video jetzt ansehen und abschicken.
        </Text>
      </View>
    )
  }
}