import React from 'react'
import { Text, View, Image, TouchableHighlight, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_NARROW_WIDTH, BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import PlayImage from '../../assets/play.png'
import CloudImage from '../../assets/cloud.png'


export default class VideoFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_NARROW_WIDTH
    const { width, height } = Image.resolveAssetSource(PlayImage)
    this.imageHeight = this.imageWidth / width * height
  }

  render() {
    return (
      <View style={[globalStyles.narrowView, { height: '48%', justifyContent: 'center' }]}>

        <Text style={globalStyles.videoText}>
          VIDEO ANSEHEN
        </Text>

        <TouchableHighlight
          style={[globalStyles.videoButton, { marginBottom: 15 }]} 
          onPress={this.props.onVideoPlayPress} 
          underlayColor={BUTTON_UNDERLAY_COLOR}>

          <Image source={PlayImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
        </TouchableHighlight>


        <Text style={[globalStyles.videoText, {width: '140%', marginLeft: 0}]}>
          VIDEO INS PROJEKT LADEN
        </Text>

        
        <TouchableHighlight 
          style={[globalStyles.videoButton, { marginBottom: 0 }]} 
          onPress={this.props.onVideoUpload} 
          underlayColor={BUTTON_UNDERLAY_COLOR}>

          <Image source={CloudImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
        </TouchableHighlight>

      </View>
    )
  }
}

VideoFooter.propTypes = {
  onVideoPlayPress: PropTypes.func.isRequired,
  onVideoUpload: PropTypes.func.isRequired
}