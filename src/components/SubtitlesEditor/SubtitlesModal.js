import React from 'react'
import { Modal, View, TextInput, TouchableHighlight, Image, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import BackImage from '../../assets/back.png'
import NextImage from '../../assets/next.png'
import Keyboard from '../Keyboard'


export default class SubtitlesModal extends React.Component {
  constructor() {
    super()

    this.state = {
      visible: false,
      currentSubtitleIndex: 0,
      value: `Multiline 
text 
t
edit
quickly`,
      subtitles: [`Multiline 
text 
t
edit
quickly`, "another subtitle", "yet another subt"]
    }

    this.imageWidth = Dimensions.get('window').width / 2.5
    const { width, height } = Image.resolveAssetSource(BackImage)
    this.imageHeight = this.imageWidth / width * height
  }

  show = () => {
    this.setState({ visible: true })
  }

  hide = () => {
    this.setState({ visible: false })
  }

  onReturn = () => {
    this.hide()
    this.props.goBack()
  }
  
  onChangeText = value => {
    this.setState({value})
  }

  nextSubtitle = () => {
    const subs = [...this.state.subtitles]
    subs[this.state.currentSubtitleIndex] = this.state.value

    if (subs.length >= currentSubtitleIndex + 1) {
      this.setState({
        subtitles: subs,
        currentSubtitleIndex
      })
    } else {
      this.setState({
        subtitles: subs,
        currentSubtitleIndex: (currentSubtitleIndex + 1)
      })
    }
  }

  render() {
    return (
      <Modal animationType="slide"
        visible={this.state.visible}
        transparent={true}
        ref="modal"
        onRequestClose={this.hide}>
        
        <Keyboard> 
          <View style={{position: 'absolute', width: '100%', height: '100%'}}>
            <View style={[globalStyles.modal, {height: 300, flexDirection: 'column', marginTop: 30}]}>
              <View style={{flex: 1, paddingTop: 15, paddingBottom: 20}}>

                <TextInput style={globalStyles.modalInput} 
                  value={this.state.value}
                  caretHidden={false}
                  multiline={true}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  onChangeText={this.onChangeText} />

              </View>
            </View>

            <View style={{height: '20%', width: '100%', position: 'absolute', top: 330, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}} >
              <TouchableHighlight 
                style={{width: this.imageWidth, height: this.imageHeight, borderRadius: 40}} 
                underlayColor={BUTTON_UNDERLAY_COLOR}
                onPress={this.onReturn}>

                <Image source={BackImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
              </TouchableHighlight>

              <TouchableHighlight 
                style={{width: this.imageWidth, height: this.imageHeight, borderRadius: 40}} 
                underlayColor={BUTTON_UNDERLAY_COLOR}
                onPress={this.nextSubtitle}>

                <Image source={NextImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
              </TouchableHighlight>
            </View>
          </View>
        </Keyboard>
      </Modal>
    )
  }
}

SubtitlesModal.propTypes = {
  // onCheckCode: PropTypes.func.isRequired,
  // onCodeSubmit: PropTypes.func.isRequired
}