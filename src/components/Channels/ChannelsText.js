import React from 'react'
import { Text, View } from 'react-native'
import { globalStyles } from '../../config/global-styles'


export default class ChannelsText extends React.Component {
  render() {
    return (
      <View style={[globalStyles.narrowView, { height: '52%', justifyContent: 'center', paddingTop: 20 }]}>
        <Text style={[globalStyles.text, {paddingBottom: 5}]}>
          Bitte wähle den Kanal/ die Kanäle aus, in denen Dein Beitrag veröffentlicht werden soll und erteile dann die Freigabe zur Veröffentlichung.
        </Text>
      </View>
    )
  }
}