import React from 'react'
import { View, Image, TouchableHighlight, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { globalStyles, BUTTON_FULL_WIDTH, BUTTON_UNDERLAY_COLOR } from '../../config/global-styles'
import YoutubeImage from '../../assets/youtube.png'
import FacebookImage from '../../assets/facebook.png'
import ApproveImage from '../../assets/approve.png'
import CheckboxChecked from '../../assets/checkbox-checked.png'
import CheckboxUnchecked from '../../assets/checkbox-unchecked.png'


export default class ChannelsFooter extends React.Component {
  constructor() {
    super()

    this.imageWidth = Dimensions.get('window').width * BUTTON_FULL_WIDTH
    const { width, height } = Image.resolveAssetSource(YoutubeImage)
    this.imageHeight = this.imageWidth / width * height
  }

  onToggleChannel = channelName => {
    let selectedChannels = [...this.props.selectedChannels]
    if (this.props.selectedChannels.includes(channelName)) {
      selectedChannels = selectedChannels.filter(channel => channel !== channelName)
    } else {
      selectedChannels.push(channelName)
    }

    this.props.updateSelectedChannels(selectedChannels)
  }

  render() {
    return (
      <View style={[globalStyles.footerContainer, { height: '48%', position: 'absolute', bottom: 0}]}>

        <View style={[globalStyles.optionButton, {flexDirection: 'row', height: this.imageHeight}]}> 
          <View style={{ width: this.imageHeight, height: this.imageHeight, padding: 3, marginRight: 3 }}>
            <Image 
              source={this.props.selectedChannels.includes("facebook") ? CheckboxChecked : CheckboxUnchecked} 
              style={{width: '100%', height: '100%'}} />
          </View>

          <View>
            <TouchableHighlight
              style={{borderRadius: 40}}
              onPress={() => this.onToggleChannel("facebook")}
              underlayColor={BUTTON_UNDERLAY_COLOR}>

              <Image source={FacebookImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
            </TouchableHighlight>
          </View>
        </View>

        <View style={[globalStyles.optionButton, {flexDirection: 'row', height: this.imageHeight}]}>
          <View style={{ width: this.imageHeight, height: this.imageHeight, padding: 3, marginRight: 3 }}>
            <Image 
              source={this.props.selectedChannels.includes("youtube") ? CheckboxChecked : CheckboxUnchecked}
              style={{width: '100%', height: '100%'}} />
          </View>

          <View>
            <TouchableHighlight
              style={{borderRadius: 40}}
              onPress={() => this.onToggleChannel("youtube")}
              underlayColor={BUTTON_UNDERLAY_COLOR}>

              <Image source={YoutubeImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
            </TouchableHighlight>
          </View>
        </View>

         <View>
          <TouchableHighlight
            style={globalStyles.optionButton} 
            onPress={this.props.publish} 
            underlayColor={BUTTON_UNDERLAY_COLOR}>

            <Image source={ApproveImage} style={{ width: this.imageWidth, height: this.imageHeight }} />
          </TouchableHighlight>
        </View>

      </View>
    )
  }
}

ChannelsFooter.propTypes = {
  selectedChannels: PropTypes.array,
  updateSelectedChannels: PropTypes.func.isRequired,
  publish: PropTypes.func.isRequired
}
