import React from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';
import { COLOR_PRIMARY } from '../config/colors';

export class SimpleEditor extends React.Component {
    render() {
        let props = {
            autoFocus: true,
            value: this.props.value,
            multiline: this.props.multiline === true,
            returnKeyType: 'done',
            underlineColorAndroid: 'transparent',
            onSubmitEditing: this.onSubmitEditing.bind(this),
            onChangeText: this.onChangeText.bind(this),
        };

        props.style = props.multiline ?
            [styles.input, { height: 200 }] :
            styles.input;

        return (
            <View>
                <Text style={styles.text}>{this.props.title}</Text>
                <View style={styles.inputContainer}>
                    <TextInput {...props} />
                </View>
                <Button style={styles.button} title="Speichern" onPress={this.onSubmitEditing.bind(this)}/>
            </View>
        );
    }

    onChangeText(value) {
        this.props.onChange(value, false);
    }

    onSubmitEditing() {
        this.props.onChange(this.props.value, true);
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        borderWidth: 2,
        borderColor: COLOR_PRIMARY,
        marginBottom: 20,
    },
    input: {
        fontSize: 20,
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        color: COLOR_PRIMARY,
        marginBottom: 20,
    }
});
