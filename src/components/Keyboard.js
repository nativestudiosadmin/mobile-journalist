import React from 'react'
import { KeyboardAvoidingView } from 'react-native'


export default class Keyboard extends React.Component {
  render() {
    return (
      <KeyboardAvoidingView behavior="height" style={{ flex: 1 }} keyboardVerticalOffset={-500} enabled>
        {this.props.children}
      </KeyboardAvoidingView>
    )
  }
}