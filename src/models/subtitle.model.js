import { Model } from './model';

export class Subtitle extends Model {
    static schema = {
        name: 'Subtitle',
        primaryKey: 'id',
        properties: {
            id: { type: 'string', optional: true },
            value: { type: 'string', optional: true },
        }
    };
}
