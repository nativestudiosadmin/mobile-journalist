import { Model } from './model';
import { NEXXTV_API } from '../config/api';

export class Video extends Model {
    static schema = {
        name: 'Video',
        primaryKey: 'id',
        properties: {
            id: { type: 'int' },
            remoteId: { type: 'string', optional: true },
            uri: { type: 'string', optional: true },
            publicUri: { type: 'string', optional: true },
            code: { type: 'string', optional: true },
            state: { type: 'string', default: 'initiated' },
            title: { type: 'string', optional: true },
            description: { type: 'string', optional: true },
            createdAt: { type: 'date', optional: true },
        }
    };

    async getStatus() {
        const body = new FormData();

        body.append('cmd', 'getstatus');
        body.append('authkey', this.code);
        body.append('item', this.remoteId);

        return fetch(NEXXTV_API, {
            method: 'post',
            body,
        }).then(response => response.json());
    }

    async getCaptions() {
        const body = new FormData();

        body.append('cmd', 'getcaptions');
        body.append('authkey', this.code);
        body.append('item', this.remoteId);

        return fetch(NEXXTV_API, {
            method: 'post',
            body,
        }).then(response => response.json());
    }

    async getMetaData() {
        const body = new FormData();

        body.append('cmd', 'getmetadata');
        body.append('authkey', this.code);
        body.append('item', this.remoteId);

        return fetch(NEXXTV_API, {
            method: 'post',
            body,
        }).then(response => response.json());
    }
}
